gl.setup(NATIVE_WIDTH, NATIVE_HEIGHT)

local json = require "json"
local font = resource.load_font("arial.ttf")
local white = resource.create_colored_texture(1,1,1,1)

local eventfeed
local passfeed
local base_time = N.base_time or 0

util.data_mapper{
    ["clock/set"] = function(time)
        base_time = tonumber(time) - sys.now()
        N.base_time = base_time
        print(tonumber(time))
        -- print(base_time)
        -- print(N.base_time)
    end;
}

function rPrint(s, l, i) -- recursive Print (structure, limit, indent)
	l = (l) or 100; i = i or "";	-- default item limit, indent string
	if (l<1) then print "ERROR: Item limit reached."; return l-1 end;
	local ts = type(s);
	if (ts ~= "table") then print (i,ts,s); return l-1 end
	print (i,ts);           -- print "table"
	for k,v in pairs(s) do  -- print "[KEY] VALUE"
		l = rPrint(v, l, i.."\t["..tostring(k).."]");
		if (l < 0) then break end
	end
	return l
end

function SecondsToClock(seconds)
  local seconds = tonumber(seconds)

  if seconds <= 0 then
    return "00:00:00";
  else
    hours = string.format("%02.f", math.floor(seconds/3600));
    mins = string.format("%02.f", math.floor(seconds/60 - (hours*60)));
    secs = string.format("%02.f", math.floor(seconds - hours*3600 - mins *60));
    return hours..":"..mins..":"..secs
  end
end

function disp_time(time)
  local days = floor(time/86400)
  local hours = floor(mod(time, 86400)/3600)
  local minutes = floor(mod(time,3600)/60)
  local seconds = floor(mod(time,60))
  return format("%d:%02d:%02d:%02d",days,hours,minutes,seconds)
end

function draw_info()
    local time = base_time + sys.now()
    local s = HEIGHT/10

    local UTC_font_sieze = 120
    local LOCAL_font_sieze = 70

    local date = os.date("%Y%m%d%H%M%S",time)

    font:write(5, 5, "UTC: " .. os.date("%j  %X", time), UTC_font_sieze, 1,1,1,1)
    font:write(5, 5+UTC_font_sieze, os.date("%a %d %b %X", time), LOCAL_font_sieze, 1,1,1,1)

    local PROJECT_TIME_font_sieze = 70
    local PROJECT_NAME_font_sieze = 120

    local date = os.date("%Y%m%d%H%M%S",time)

    font:write(WIDTH-650, 5, "MET: " .. os.date("%j  %X", time), PROJECT_TIME_font_sieze, 1,1,1,1)
    font:write(WIDTH-900, 5+PROJECT_TIME_font_sieze, "PROJECT: EDC", PROJECT_NAME_font_sieze, 1,1,1,1)

    local events_y = HEIGHT/10*1.6
    local passes_y = HEIGHT/2

    local events_font_sieze = 60
    local events_to_display = math.floor((HEIGHT - events_y - passes_y) / events_font_sieze)
    local events_margin_x = 10
    local events_margin_y = ((HEIGHT - events_y - passes_y) - (events_to_display * events_font_sieze)) / (events_to_display + 1)

    white:draw(0, events_y-2, WIDTH, events_y+2, 1)

    for i,line in pairs(eventfeed) do
      if i > events_to_display then break end
      local station_width = font:write(events_margin_x, events_y+(i-1)*events_font_sieze+events_margin_y*(i), line['station'], events_font_sieze, 1,1,1,1)
      local support_id_width = font:write(events_margin_x+WIDTH/10*2, events_y+(i-1)*events_font_sieze+events_margin_y*(i), line['support_id'], events_font_sieze, 1,1,1,1)
      local support_id_width = font:write(events_margin_x+WIDTH/10*3.5, events_y+(i-1)*events_font_sieze+events_margin_y*(i), line['activity'], events_font_sieze, 1,1,1,1)

      local start_time = makeTimeStamp(line['start_time'])
      local stop_time = makeTimeStamp(line['stop_time'])
      local time_to_event = time-start_time
      -- print(time_to_event)
      if start_time > time then
        local start_time_width = font:write(events_margin_x+WIDTH/10*8, events_y+(i-1)*events_font_sieze+events_margin_y*(i), os.date('%H:%M:%S', time_to_event), events_font_sieze, 1,1,1,1)
      elseif start_time < time then
        local start_time_width = font:write(events_margin_x+WIDTH/10*8, events_y+(i-1)*events_font_sieze+events_margin_y*(i), os.date('%H:%M:%S', stop_time-time_to_event), events_font_sieze, 1,1,1,1)
      end
      -- local start_time_width = font:write(events_margin_x*10, events_y+(i-1)*events_font_sieze+events_margin_y*(i), , events_font_sieze, 1,1,1,1)
      -- local stop_time_width = font:write(events_margin_x*15, events_y+(i-1)*events_font_sieze+events_margin_y*(i), line['stop_time'], events_font_sieze, 1,1,1,1)
    end

    local passes_font_sieze = 80
    local passes_to_display = math.floor((HEIGHT - passes_y) / passes_font_sieze)
    local passes_margin_x = 10
    local passes_margin_y = ((HEIGHT - passes_y) - (passes_to_display * passes_font_sieze)) / (passes_to_display + 1)

    white:draw(0, passes_y-2, WIDTH, passes_y+2, 1)

    for i,line in pairs(passfeed) do
      if i > passes_to_display then break end
      local station = font:write(passes_margin_x, passes_y+(i-1)*passes_font_sieze+passes_margin_y*(i), line['station'], passes_font_sieze, 1,1,1,1)
      local support_id = font:write(passes_margin_x+WIDTH/10*1.5, passes_y+(i-1)*passes_font_sieze+passes_margin_y*(i), line['support_id'], passes_font_sieze, 1,1,1,1)
      local max_elevation = font:write(passes_margin_x+WIDTH/10*3, passes_y+(i-1)*passes_font_sieze+passes_margin_y*(i), line['max_elevation'] .. "°", passes_font_sieze, 1,1,1,1)

      local start_time = makeTimeStamp(line['start_time'])
      local stop_time = makeTimeStamp(line['stop_time'])
      local time_to_event = time-start_time
      -- print(time_to_event)

      if start_time > time then
        local start_time_width = font:write(passes_margin_x+WIDTH/10*5,  passes_y+(i-1)*passes_font_sieze+passes_margin_y*(i), os.date('%H:%M:%S', time_to_event), passes_font_sieze, 1,1,1,1)
        local stop_time_width = font:write(passes_margin_x+WIDTH/10*8, passes_y+(i-1)*passes_font_sieze+passes_margin_y*(i),os.date('%H:%M:%S', stop_time-start_time), passes_font_sieze, 1,1,1,1)
      elseif start_time < time then
        local start_time_width = font:write(passes_margin_x+WIDTH/10*5,  passes_y+(i-1)*passes_font_sieze+passes_margin_y*(i), os.date('%H:%M:%S', stop_time-time_to_event), passes_font_sieze, 1,1,1,1)
        local stop_time_width = font:write(passes_margin_x+WIDTH/10*8, passes_y+(i-1)*passes_font_sieze+passes_margin_y*(i),os.date('%H:%M:%S', stop_time-start_time), passes_font_sieze, 1,1,1,1)
      end

      -- local start_time = font:write(passes_margin_x+WIDTH/10*5, passes_y+(i-1)*passes_font_sieze+passes_margin_y*(i), line['start_time'], passes_font_sieze, 1,1,1,1)
      -- local stop_time = font:write(passes_margin_x+WIDTH/10*8, passes_y+(i-1)*passes_font_sieze+passes_margin_y*(i), line['stop_time'], passes_font_sieze, 1,1,1,1)
    end
end

function makeTimeStamp(dateString)
    --     local pattern = "(%d+)%-(%d+)%-(%d+)T(%d+):(%d+):(%d+)([%+%-])(%d+)%:(%d+)"
    local pattern = "(%d+)%-(%d+)%-(%d+)T(%d+):(%d+):(%d+)"
    local xyear, xmonth, xday, xhour, xminute, xseconds = dateString:match(pattern)
    local convertedTimestamp = os.time({year = xyear, month = xmonth, day = xday, hour = xhour, min = xminute, sec = xseconds})
    -- local offset = xoffsethour * 60 + xoffsetmin
    -- if xoffset == "-" then offset = offset * -1 end
    -- return convertedTimestamp + offset
    return convertedTimestamp
end

function convert_time(time)

end

function node.render()
    gl.clear(0,0,1,1)
    draw_info()
end

util.json_watch("eventfeed.json", function(raw)
    eventfeed = raw
    -- eventfeed = json.decode(raw)
    -- rPrint(eventfeed,nil,"test")
    -- settings contains the decoded json data as a Lua data structure
end)

util.json_watch("passfeed.json", function(raw)
    passfeed = raw
    -- passfeed = json.decode(raw)
    -- settings contains the decoded json data as a Lua data structure
end)
