local api, CHILDS, CONTENTS = ...

local M = {}

local feed = {}
local font = resource.load_font(api.localized "arial.ttf")
local global_config = {}

local function wrap(str, font, size, max_w)
    local lines = {}
    local space_w = font:width(" ", size)

    local remaining = max_w
    local line = {}
    for non_space in str:gmatch("%S+") do
        local w = font:width(non_space, size)
        if remaining - w < 0 then
            lines[#lines+1] = table.concat(line, "")
            line = {}
            remaining = max_w
        end
        line[#line+1] = non_space
        line[#line+1] = " "
        remaining = remaining - w - space_w
    end
    if #line > 0 then
        lines[#lines+1] = table.concat(line, "")
    end
    return lines
end

function M.task(starts, ends, config, x1, y1, x2, y2)
    local w, h = x2-x1, y2-y1
    local items = {}

    local auto = global_config.auto_align
    local size = global_config.font_size -- font size
    local image_w = global_config.image_w
    local image_h = global_config.image_h
    local margin = global_config.image_m

    local new_image_h  = (y2-y1) / (table.getn(items))  - (( (table.getn(items)+2) * margin ) / (table.getn(items)+1))
    local new_image_w = ( (new_image_h) / 9 ) * 16

    print( w-new_image_w-margin-margin)

    for _, entry in ipairs(feed) do
        items[#items+1] = {
            image = resource.load_image(entry.image:copy()),
            lines = wrap(entry.text, font, size, w-new_image_w-margin-margin),
        }
    end

    local y = y1
    local x = x1

    if auto then
	-- print (new_image_h)
	-- print (table.getn(items))
        for now in api.frame_between(starts, ends) do
            print( w-new_image_w-margin-margin)
            for _, item in ipairs(items) do
		y = (_-1) * (new_image_h + margin)
                util.draw_correct(item.image, x+margin, y+margin, x+new_image_w+margin, y+new_image_h+margin)
                for i, line in ipairs(item.lines) do
			-- if a[i] == v then break end
			font:write(x+new_image_w+margin+margin, y+(i-1)*(size+5), line, size, 1,1,1,1)
                end
                -- y = y + math.max(image_h, #item.lines*(size+5)) + 10
            end
        end
    else
	--[[
        for now in api.frame_between(starts, ends) do
            for _, item in ipairs(items) do
                -- print(item.image:size())
                util.draw_correct(item.image, x, y, x+image_w, y+image_h)
                for i, line in ipairs(item.lines) do
                    font:write(x+image_w+margin, y+(i-1)*(size+5), line, size, 1,1,1,1)
                end
                y = y + math.max(image_h, #item.lines*(size+5)) + 10
            end
        end
	--]]
    end
end

function M.updated_config_json(new_config)
    global_config = new_config
end

function M.updated_feed_json(new_feed)
    print "updated feed"
    for _, entry in ipairs(new_feed) do
        entry.image = resource.open_file(api.localized(entry.image))
    end

    feed = new_feed
    pp(feed)
end

return M
